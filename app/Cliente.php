<?php

namespace App;

use App\User;
use App\Telefone;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
	//white list para proteger contra envio em massa:
    protected $fillable = ['nome','email','data_nascimento','endereco','cep','cidade','estado']; 

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'int',
    ];


    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function telefone()
    {
    	return $this->hasMany(Telefone::class, 'foreign_key');
    }
}
