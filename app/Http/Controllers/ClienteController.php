<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ClienteRepository;
use App\Repositories\TelefoneRepository;
use Carbon\Carbon; //para manipulacao de datas

use App\Cliente;
use App\Telefone;

class ClienteController extends Controller
{
    protected $clientes; //dando o hint do repositorio onde pega dados do banco, veja conteudo do construtor
    protected $telefones;


    public function __construct(ClienteRepository $clientes, TelefoneRepository $telefones) 
    {
    	$this->middleware('auth'); //precisa ser autenticado para usar os metodos da classe

    	$this->clientes = $clientes;
    	$this->telefones = $telefones;
    }


    /**
     * Exibe todos os clientes do usuario registrado
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    	return view('clientes.index', [
    		'clientes' => $this->clientes->forUser($request->user()),
    		'base_url' => url('/'),
    	]);
    }



    /**
     * Formulario de cadastro
     * @param Request $request
     * @return Response
     */
    public function showFormCliente(Request $request)
    {
    	return view('clientes.new',[
    		'base_url'	=> url('/'),
    	]);
    }


    /**
     * Cadastra um cliente     
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' 		 => 'required|max:255',
            'email' 	 => 'required|email|unique:clientes',
            'nascimento' => 'required|date_format:d/m/Y',            
        ]);

        $clienteSaved = $request->user()->clientes()->create([
            'nome' 		 	  => $request->nome,
            'email' 	 	  => $request->email,
            'data_nascimento' => Carbon::createFromFormat('d/m/Y', $request->nascimento),
            'endereco' 	 	  => $request->endereco,
            'cep' 	 	 	  => $request->cep,
            'cidade' 	 	  => $request->cidade,
            'estado' 	 	  => $request->estado,
        ]);

        if( ! empty($request->telefones)) {
        
        	foreach ($request->telefones as $numeroTelefone) {

        		Telefone::create([
		        	'cliente_id' => $clienteSaved->id,
		        	'numero' 	 => $numeroTelefone,
	        	]);
        	}
        	
        }

        //return redirect('/clientes');
        return view('clientes.index',[
        	'clientes' => $this->clientes->forUser($request->user()),
    		'base_url'	=> url('/'),
    		'mensagem' 	=> array(
    				'tipo'		=> 'success',
    				'conteudo'	=> 'Cadastro realizado com sucesso'	
    			),
    	]);
    }

    /**
     * Exclui um determinado cliente     
     * @param  Request  $request
     * @param  Cliente  $cliente
     * @return Response
     */
    public function destroy(Request $request, Cliente $cliente)
    {
        $this->authorize('destroy', $cliente); //regra exclusao em App\Policies\ClientePolicy ativado em App\Providers\AuthServiceProvider

        $cliente->delete();

        return redirect('/clientes');
    }


    public function edit(Request $request, $id)
    {
        
        $clienteFound = $this->clientes->getcliente($request->user(), $id);
        $telefonesFound = $this->telefones->forCliente($id);

        return view('clientes.edit', [
            'cliente' => $clienteFound,
            'telefones' => $telefonesFound,
            'base_url' => url('/'),
        ]);
        
    }


    public function updateCliente(Request $request)
    {
    	$data = array(
    		'nome' 		 	  => $request->nome,
            'email' 	 	  => $request->email,
            'data_nascimento' => Carbon::createFromFormat('d/m/Y', $request->nascimento),
            'endereco' 	 	  => $request->endereco,
            'cep' 	 	 	  => $request->cep,
            'cidade' 	 	  => $request->cidade,
            'estado' 	 	  => $request->estado,
            );
    	Cliente::find($request->id)->update($data);

    	//atualizando telefones:
    	if( ! empty($request->telefones) ) {        	

        	Telefone::where('cliente_id', $request->id)->delete();
        	        	        	
        	foreach ($request->telefones as $numeroTelefone) {

        		Telefone::create([
		        	'cliente_id' => $request->id,
		        	'numero' 	 => $numeroTelefone,
	        	]);
        	}
        	
        }

        return view('clientes.index',[
        	'clientes' => $this->clientes->forUser($request->user()),
    		'base_url'	=> url('/'),
    		'mensagem' 	=> array(
    				'tipo'		=> 'success',
    				'conteudo'	=> 'Cadastro do cliente nr. '.$request->id.' - '.$request->nome.' atualizado com sucesso'	
    			),
    	]);
    }

}
