<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', function () {
	return view('welcome');
})->middleware('guest');

Route::get('/clientes', 'ClienteController@index');
Route::post('/cliente', 'ClienteController@store');

Route::post('/cliente/{cliente}/edit', 'ClienteController@edit');

Route::post('/cliente/new', 'ClienteController@showFormCliente');
Route::get('/cliente/new', 'ClienteController@showFormCliente');

Route::post('/cliente/update', 'ClienteController@updateCliente');

Route::delete('/cliente/{cliente}', 'ClienteController@destroy');
Route::auth();

Route::get('/home', 'HomeController@index');
