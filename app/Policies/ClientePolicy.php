<?php

namespace App\Policies;

use App\User;
use App\Cliente;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClientePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determina se o usuario pode excluir o cadastro do cliente determinado
     * @param User $user
     * @param Cliente $cliente
     * @return bool
     */
    public function destroy(User $user, Cliente $cliente)
    {
        return $user->id === $cliente->user_id;
    }
}