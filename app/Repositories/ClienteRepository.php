<?php

namespace App\Repositories;

use App\User;
use App\Cliente;


class ClienteRepository
{
 	/**
	 * Retorna todos os clientes cadastros para o usuario	 
	 * @param  User  $user
	 * @return Collection
	 */
    public function forUser(User $user)
    {
        return Cliente::where('user_id', $user->id)
                    ->orderBy('nome', 'asc')
                    ->get();
    }


    /**
     * Retorna uma linha de um cliente em especifico, cadastrado para o usuario logado
     * @param User $user
     * @param Integer $id
     * @return Row
     */ 
    public function getCliente(User $user, $id)
    {        
        return Cliente::where([
            ['user_id', $user->id],
            ['id', $id],
            ])->first();
    }
}