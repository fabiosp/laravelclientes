<?php

namespace App\Repositories;

use App\Cliente;
use App\Telefone;

class TelefoneRepository
{
 	/**
	 * Retorna todos os telefones cadastros para um cliente	 
	 * @param  Cliente  $cliente
	 * @return Collection
	 */
    public function forCliente($id)
    {
        return Telefone::where('cliente_id', $id)->get();
    }


    /**
     * Retorna um telefone de um cliente em especifico
     * @param Cliente $cliente
     * @param Integer $id
     * @return Row
     */ 
    public function getTelefone(Cliente $cliente)
    {        
        return Telefone::where([
            ['cliente_id', $cliente->id],
            ])->first();
    }

    public function destroyMany($id)
{
    Telefone::destroy($id);
    // redirect or whatever...
}
}