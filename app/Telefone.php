<?php

namespace App;

use App\Cliente;
use Illuminate\Database\Eloquent\Model;

class Telefone extends Model
{
    protected $fillable = ['cliente_id','numero'];
    public $timestamps = false; //


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'cliente_id' => 'int',
    ];


    public function cliente()
    {
    	return $this->belongsTo(Cliente::class);
    }
}
