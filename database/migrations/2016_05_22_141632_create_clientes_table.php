<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index(); 
            $table->string('nome',255);
            $table->string('email',200);
            $table->date('data_nascimento');
            $table->string('endereco',255)->nullable();
            $table->string('cep',10)->nullable();
            $table->string('cidade',255)->nullable();
            $table->string('estado',2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clientes');
    }
}
