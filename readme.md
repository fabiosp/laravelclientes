# Laravel PHP Framework - Cadastro de Clientes


Aproveitando o teste da Designio, o CRUD de clientes foi feito no Laravel! Mais de 16 horas de trabalho envolvendo o entendimento sobre o framework mais o front-end e back-end da aplicação. O cadastro exemplo foi implementando conforme os campos especificados em e-mail. A modelagem da base e a aplicação permite um número indefinido de telefones para o cliente.


## Back-end
* Framework Laravel 5.2
* Serviço de autenticação do Laravel
* Base de dados em MySQL
* Rotas para satisfazer o CRUD de Clientes
* Migrations e Eloquent (ORM) para gerar os blueprints e manipulações com banco de dados
* Muita leitura em [Laravel Docs](http://laravel.com/docs)

## Front-end

* Bootstrap
* Datepicker
* DataTables completo (busca e paginação)

## Instalando... em 8 comandos!

```bash
$ git clone https://fabiosp@bitbucket.org/fabiosp/laravelclientes.git
$ cd laravelclientes
$ composer install
$ sudo chmod -R 777 bootstrap/cache/
$ sudo chmod -R 777 storage/
$ cp .env.example .env
//criar banco de dados vazio em MySQL, depois, no arquivo .env setar o nome do banco, usuário e senha
$ php artisan key:generate
$ php artisan migrate
```

Sim, é necessário fornecer permissão para os diretórios bootstrap/cache e storage/ para que o Laravel funcione corretamente.

Caso encontrar alguns problemas com o artisan migrate ou deseja criar uma base inicial, existe na raiz do projeto um scritp SQL de exemplo. Neste script possui um usuário registrado. 

* usuário: admin@designio.com.br
* senha: admin


Não sabe instalar [Composer](https://getcomposer.org/) e quer uma ajuda na sua máquina linux? Então vamos lá:
```bash
$ curl -sS https://getcomposer.org/installer | php
$ sudo mv composer.phar /usr/local/bin/composer
$ sudo chmod +x /usr/local/bin/composer
```

## Screenshots

[Landing Page](public/img/inicio.png)

[Listando](public/img/lista.png)

[Editando](public/img/editando.png)


## ToDo
* Corrigir com flash data e redirect() as requisições para evitar falhas de navegação deixadas pelos Responses das Views.
* Desenvolver mais testes (configurar PHPUnit)


