@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-sm-offset-2 col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                Editando Cliente
            </div>

            <div class="panel-body">
                <!-- Display Validation Errors -->
                @include('common.errors')

                <form action="{{ $base_url }}/cliente/update" method="POST" class="form-horizontal">
                    {{ csrf_field() }}

                    <input type="hidden" name="id" value="{{ $cliente->id }}" />

                    <div class="form-group">
                        <label for="cliente-datacricao" class="col-sm-3 control-label">Criado em </label>

                        <div class="col-sm-2">
                            <input type="text" name="datacriacao" id="cliente-datacricao" disabled="disabled" value="{{ date('d/m/Y H:i:s',strtotime($cliente->created_at)) }}">

                        </div>                            
                    </div>

                    <div class="form-group">
                        <label for="cliente-dataup" class="col-sm-3 control-label">Alterado em</label>

                        <div class="col-sm-2">
                            <input type="text" name="dataup" id="cliente-dataup" disabled="disabled" value="{{ date('d/m/Y H:i:s',strtotime($cliente->updated_at)) }}">
                        </div>                            
                    </div>

                    <div class="form-group">
                        <label for="cliente-nome" class="col-sm-3 control-label">Nome</label>

                        <div class="col-sm-6">
                            <input type="text" name="nome" id="cliente-nome" class="form-control" value="{{ $cliente->nome }}">
                        </div>                            
                    </div>

                    <div class="form-group">
                        <label for="cliente-email" class="col-sm-3 control-label">E-mail</label>

                        <div class="col-sm-6">
                            <input type="text" name="email" id="cliente-email" class="form-control" value="{{ $cliente->email }}">
                        </div>                            
                    </div>

                    <div class="form-group">
                        <label for="cliente-nascimento" class="col-sm-3 control-label">Data de Nascimento</label>

                        <div class="col-sm-6">
                            <input type="text" name="nascimento" id="cliente-nascimento" class="form-control datepicker" value="{{ date('d/m/Y',strtotime($cliente->data_nascimento)) }}">
                        </div>                            
                    </div>

                    <div class="form-group">
                        <label for="cliente-endereco" class="col-sm-3 control-label">Endereço</label>

                        <div class="col-sm-6">
                            <input type="text" name="endereco" id="cliente-endereco" class="form-control" value="{{ $cliente->endereco }}">
                        </div>                            
                    </div>

                    <div class="form-group">
                        <label for="cliente-cep" class="col-sm-3 control-label">CEP</label>

                        <div class="col-sm-6">
                            <input type="text" name="cep" id="cliente-cep" class="form-control" value="{{ $cliente->cep }}">
                        </div>                            
                    </div>

                    <div class="form-group">
                        <label for="cliente-cidade" class="col-sm-3 control-label">Cidade</label>

                        <div class="col-sm-6">
                            <input type="text" name="cidade" id="cliente-cidade" class="form-control" value="{{ $cliente->cidade }}">
                        </div>                            
                    </div>

                    <div class="form-group">
                        <label for="cliente-cidade" class="col-sm-3 control-label">Estado</label>

                        <div class="col-sm-6">
                            <select name="estado" class="form-control">
                                    <option value="AC"{{ ("AC" == $cliente->estado)? ' selected=selected' : null }}>Acre</option>
                                    <option value="AL"{{ ("AL" == $cliente->estado)? ' selected=selected' : null }}>Alagoas</option>
                                    <option value="AM"{{ ("AM" == $cliente->estado)? ' selected=selected' : null }}>Amazonas</option>
                                    <option value="AP"{{ ("AP" == $cliente->estado)? ' selected=selected' : null }}>Amapá</option>
                                    <option value="BA"{{ ("BA" == $cliente->estado)? ' selected=selected' : null }}>Bahia</option>
                                    <option value="CE"{{ ("CE" == $cliente->estado)? ' selected=selected' : null }}>Ceará</option>
                                    <option value="DF"{{ ("DF" == $cliente->estado)? ' selected=selected' : null }}>Distrito Federal</option>
                                    <option value="ES"{{ ("ES" == $cliente->estado)? ' selected=selected' : null }}>Espírito Santo</option>
                                    <option value="GO"{{ ("GO" == $cliente->estado)? ' selected=selected' : null }}>Goiás</option>
                                    <option value="MA"{{ ("MA" == $cliente->estado)? ' selected=selected' : null }}>Maranhão</option>
                                    <option value="MT"{{ ("MT" == $cliente->estado)? ' selected=selected' : null }}>Mato Grosso</option>
                                    <option value="MS"{{ ("MS" == $cliente->estado)? ' selected=selected' : null }}>Mato Grosso do Sul</option>
                                    <option value="MG"{{ ("MG" == $cliente->estado)? ' selected=selected' : null }}>Minas Gerais</option>
                                    <option value="PA"{{ ("PA" == $cliente->estado)? ' selected=selected' : null }}>Pará</option>
                                    <option value="PB"{{ ("PB" == $cliente->estado)? ' selected=selected' : null }}>Paraíba</option>
                                    <option value="PR"{{ ("PR" == $cliente->estado)? ' selected=selected' : null }}>Paraná</option>
                                    <option value="PE"{{ ("PE" == $cliente->estado)? ' selected=selected' : null }}>Pernambuco</option>
                                    <option value="PI"{{ ("PI" == $cliente->estado)? ' selected=selected' : null }}>Piauí</option>
                                    <option value="RJ"{{ ("RJ" == $cliente->estado)? ' selected=selected' : null }}>Rio de Janeiro</option>
                                    <option value="RN"{{ ("RN" == $cliente->estado)? ' selected=selected' : null }}>Rio Grande do Norte</option>
                                    <option value="RO"{{ ("RO" == $cliente->estado)? ' selected=selected' : null }}>Rondônia</option>
                                    <option value="RS"{{ ("RS" == $cliente->estado)? ' selected=selected' : null }}>Rio Grande do Sul</option>
                                    <option value="RR"{{ ("RR" == $cliente->estado)? ' selected=selected' : null }}>Roraima</option>
                                    <option value="SC"{{ ("SC" == $cliente->estado)? ' selected=selected' : null }}>Santa Catarina</option>
                                    <option value="SE"{{ ("SE" == $cliente->estado)? ' selected=selected' : null }}>Sergipe</option>
                                    <option value="SP"{{ ("SP" == $cliente->estado)? ' selected=selected' : null }}>São Paulo</option>
                                    <option value="TO"{{ ("TO" == $cliente->estado)? ' selected=selected' : null }}>Tocantins</option>                                   
                            </select>                                
                        </div>                            
                    </div>


                    <div class="form-group">
                        <label for="cliente-name" class="col-sm-3 control-label">Telefones</label>

                        <div class="col-sm-6">
                            <div class="field_wrapper">                                
                                @if (count($telefones) > 0)

                                    <div>
                                        <input type="text" name="telefones[]" value="{{ $telefones[0]->numero }}"/>
                                        <a href="javascript:void(0);" class="add_button" title="Add field">                                             
                                            <button type="button" class="btn btn-link"> 
                                                <i class="fa fa-btn fa-plus-square"></i> Add
                                            </button>
                                        </a>
                                    </div>
                                    @for ($i = 1; $i < count($telefones); $i++)
                                        <div>
                                            <input type="text" name="telefones[]" value="{{ $telefones[$i]->numero }}"/>
                                            <a href="javascript:void(0);" class="remove_button" title="Remove field">
                                                <button type="button" class="btn btn-link"> 
                                                    <i class="fa fa-btn fa-minus-square"></i> Rem
                                                </button>
                                            </a>
                                        </div>                                            
                                    @endfor

                                @else

                                    <div>
                                        <input type="text" name="telefones[]" value=""/>
                                        <a href="javascript:void(0);" class="add_button" title="Add field">add</a>
                                    </div>

                                @endif                                    
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                         <div class="col-sm-offset-3 col-sm-3">
                                <a href="{{ $base_url }}/clientes">
                                    <button type="button" class="btn btn-default">
                                        <i class="fa fa-btn fa-arrow-left"></i>Voltar
                                    </button>
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-check-square-o"></i>Atualizar Cliente
                                </button>
                            </div>
                    </div>
                </form>
                <br /><br /><br />
            </div>
        </div>
@endsection
