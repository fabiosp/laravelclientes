@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">

            @if( isset($mensagem) )
                <div class="alert alert-{{ $mensagem['tipo'] }}" role="alert">
                    {{ $mensagem['conteudo'] }}
                </div>
            @endif

            <div class="form-group">
                <form action="{{ $base_url }}/cliente/new" method="POST">
                    {{ csrf_field() }}                                                

                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-plus"></i>Novo Cadastro
                    </button>
                </form>
            </div>

            <!-- clientes atuais -->
            @if (count($clientes) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Clientes atuais
                    </div>

                    <div class="panel-body">
                        
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Cliente</th>
                                    <th>E-mail</th>
                                    <th>Cidade</th>
                                    <th>UF</th> 
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>                          
                            <tbody>
                            @foreach ($clientes as $cliente)
                                <tr>
                                    <td>{{ $cliente->id }}</td>
                                    <td>{{ $cliente->nome }}</td>
                                    <td>{{ $cliente->email }}</td>
                                    <td>{{ $cliente->cidade }}</td>
                                    <td>{{ $cliente->estado }}</td>
                                    <td>
                                        <form action="{{ $base_url }}/cliente/{{ $cliente->id }}/edit" method="POST">
                                            {{ csrf_field() }}                                                

                                            <button type="submit" id="edit-task-{{ $cliente->id }}" class="btn btn-warning btn-sm">
                                                <i class="fa fa-btn fa-pencil"></i>Editar
                                            </button>
                                        </form>
                                    </td>                
                                    <td>
                                        <form action="{{ $base_url }}/cliente/{{ $cliente->id }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <button class="btn btn-danger btn-sm" type="button" data-toggle="modal" data-target="#confirmDelete" data-title="Deletar Cliente" data-message="Certeza que quer apagar este cadastro (os telefones serão excluídos)?">
                                                <i class="fa fa-btn fa-trash"></i>Deletar
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            @else
                Não existem clientes cadastrados para você
            @endif
        </div>
    </div>
@endsection
