@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Novo Cliente
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <form action="{{ $base_url }}/cliente" method="POST" class="form-horizontal">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="task-nome" class="col-sm-3 control-label">Nome</label>

                            <div class="col-sm-6">
                                <input type="text" name="nome" id="task-nome" class="form-control" value="">
                            </div>                            
                        </div>

                        <div class="form-group">
                            <label for="task-email" class="col-sm-3 control-label">E-mail</label>

                            <div class="col-sm-6">
                                <input type="text" name="email" id="task-email" class="form-control" value="">
                            </div>                            
                        </div>
    
                        <div class="form-group">
                            <label for="task-nascimento" class="col-sm-3 control-label">Data de Nascimento</label>

                            <div class="col-sm-6">
                                <input type="text" name="nascimento" id="task-nascimento" class="form-control datepicker">
                            </div>                            
                        </div>

                        <div class="form-group">
                            <label for="task-endereco" class="col-sm-3 control-label">Endereço</label>

                            <div class="col-sm-6">
                                <input type="text" name="endereco" id="task-endereco" class="form-control" value="">
                            </div>                            
                        </div>

                        <div class="form-group">
                            <label for="task-cep" class="col-sm-3 control-label">CEP</label>

                            <div class="col-sm-6">
                                <input type="text" name="cep" id="task-cep" class="form-control" value="">
                            </div>                            
                        </div>

                        <div class="form-group">
                            <label for="task-cidade" class="col-sm-3 control-label">Cidade</label>

                            <div class="col-sm-6">
                                <input type="text" name="cidade" id="task-cidade" class="form-control" value="">
                            </div>                            
                        </div>

                        <div class="form-group">
                            <label for="task-cidade" class="col-sm-3 control-label">Estado</label>

                            <div class="col-sm-6">
                                <select name="estado" class="form-control">
                                        <option value="AC">Acre</option>
                                        <option value="AL">Alagoas</option>
                                        <option value="AM">Amazonas</option>
                                        <option value="AP">Amapá</option>
                                        <option value="BA">Bahia</option>
                                        <option value="CE">Ceará</option>
                                        <option value="DF">Distrito Federal</option>
                                        <option value="ES">Espírito Santo</option>
                                        <option value="GO">Goiás</option>
                                        <option value="MA">Maranhão</option>
                                        <option value="MT">Mato Grosso</option>
                                        <option value="MS">Mato Grosso do Sul</option>
                                        <option value="MG">Minas Gerais</option>
                                        <option value="PA">Pará</option>
                                        <option value="PB">Paraíba</option>
                                        <option value="PR">Paraná</option>
                                        <option value="PE">Pernambuco</option>
                                        <option value="PI">Piauí</option>
                                        <option value="RJ">Rio de Janeiro</option>
                                        <option value="RN">Rio Grande do Norte</option>
                                        <option value="RO">Rondônia</option>
                                        <option value="RS">Rio Grande do Sul</option>
                                        <option value="RR">Roraima</option>
                                        <option value="SC">Santa Catarina</option>
                                        <option value="SE">Sergipe</option>
                                        <option value="SP">São Paulo</option>
                                        <option value="TO">Tocantins</option>                                   
                                </select>                                
                            </div>                            
                        </div>


                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Telefones</label>

                            <div class="col-sm-6">
                                <div class="field_wrapper">
                                    <div>
                                        <input type="text" name="telefones[]" value=""/>
                                        <a href="javascript:void(0);" class="add_button" title="Add field">
                                            <button type="button" class="btn btn-link">
                                                <i class="fa fa-btn fa-plus-square"></i> Add
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>                            
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-3">
                                <a href="{{ $base_url }}/clientes">
                                    <button type="button" class="btn btn-default">
                                        <i class="fa fa-btn fa-arrow-left"></i>Voltar
                                    </button>
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-plus"></i>Gravar Cliente
                                </button>
                            </div>
                        </div>                        
                    </form>
                </div>
            </div>
@endsection
